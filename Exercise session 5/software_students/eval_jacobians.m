function [dfdx,dfdu] = eval_jacobians(x,u,Ts)
    %% Parameters
    m    = 1.4;   % [kg]         Drone mass
    L    = 0.2;   % [m]          Distance motor - drone center
    Ix   = 0.03;  % [kg*m^2]     Moment of inertia around x-axis (roll)
    Iy   = 0.03;  % [kg*m^2]     Moment of inertia around y-axis (pitch)
    Iz   = 0.04;  % [kg*m^2]     Moment of inertia around z-axis (yaw)
    Kyaw = 4;     % [rad*m]      Constant relating propulsion force to propeller moment on frame

    
    %% Jacobian wrt states df/dx
    dfdx = zeros(length(x), length(x));
    
    dfdx(1, 4) = 1;
    
    dfdx(2, 5) = 1;
    
    dfdx(3, 6) = 1;
    
    dfdx(4, 7) = 1/m*sum(u)*(-sin(x(7))*sin(x(8))*cos(x(9))+cos(x(7))*sin(x(9)));
    dfdx(4, 8) = 1/m*sum(u)*(cos(x(7))*cos(x(8))*cos(x(9)));
    dfdx(4, 9) = 1/m*sum(u)*(-cos(x(7))*sin(x(8))*sin(x(9))+sin(x(7))*cos(x(9)));
    
    dfdx(5, 7) = 1/m*sum(u)*(-sin(x(7))*sin(x(8))*sin(x(9))-cos(x(7))*cos(x(9)));
    dfdx(5, 8) = 1/m*sum(u)*(cos(x(7))*cos(x(8))*sin(x(9)));
    dfdx(5, 9) = 1/m*sum(u)*(cos(x(7))*sin(x(8))*cos(x(9))+sin(x(7))*sin(x(9)));
    
    dfdx(6, 7) = 1/m*sum(u)*(-sin(x(7))*cos(x(8)));
    dfdx(6, 8) = 1/m*sum(u)*(-cos(x(7))*sin(x(8)));
    
    dfdx(7, 10) = 1;
    
    dfdx(8, 11) = 1;
    
    dfdx(9, 12) = 1;
    
    dfdx(10, 11) = (Iy-Iz)/Ix*x(12);
    dfdx(10, 12) = (Iy-Iz)/Ix*x(11);
    
    dfdx(11, 10) = (Iz-Ix)/Iy*x(12);
    dfdx(11, 12) = (Iz-Ix)/Iy*x(10);
    
    dfdx(12, 10) = (Ix-Iy)/Iz*x(11);
    dfdx(12, 11) = (Ix-Iy)/Iz*x(10);
    
    %% Jacobian wrt inputs df/du
    dfdu = zeros(length(x), length(u));
    dfdu(4, 1) = 1/m*(cos(x(7))*sin(x(8))*cos(x(9))+sin(x(7))*sin(x(9)));
    dfdu(4, 2) = 1/m*(cos(x(7))*sin(x(8))*cos(x(9))+sin(x(7))*sin(x(9)));
    dfdu(4, 3) = 1/m*(cos(x(7))*sin(x(8))*cos(x(9))+sin(x(7))*sin(x(9)));
    dfdu(4, 4) = 1/m*(cos(x(7))*sin(x(8))*cos(x(9))+sin(x(7))*sin(x(9)));
    
    dfdu(5, 1) = 1/m*(cos(x(7))*sin(x(8))*sin(x(9))-sin(x(7))*cos(x(9)));
    dfdu(5, 2) = 1/m*(cos(x(7))*sin(x(8))*sin(x(9))-sin(x(7))*cos(x(9)));
    dfdu(5, 3) = 1/m*(cos(x(7))*sin(x(8))*sin(x(9))-sin(x(7))*cos(x(9)));
    dfdu(5, 4) = 1/m*(cos(x(7))*sin(x(8))*sin(x(9))-sin(x(7))*cos(x(9)));
    
    dfdu(6, 1) = 1/m*(cos(x(7))*cos(x(8)));
    dfdu(6, 2) = 1/m*(cos(x(7))*cos(x(8)));
    dfdu(6, 3) = 1/m*(cos(x(7))*cos(x(8)));
    dfdu(6, 4) = 1/m*(cos(x(7))*cos(x(8)));
    
    dfdu(10, 3) = L/Ix;
    dfdu(10, 4) = -L/Ix;
    
    dfdu(11, 1) = L/Iy;
    dfdu(11, 2) = -L/Iy;
    
    dfdu(12, 1) = Kyaw/Iz;
    dfdu(12, 2) = Kyaw/Iz;
    dfdu(12, 3) = -Kyaw/Iz;
    dfdu(12, 4) = -Kyaw/Iz;
    

    %% Discretize with Forward Euler scheme
    dfdx = eye(length(x)) + Ts*dfdx;
    
    dfdu = dfdu*Ts;

end