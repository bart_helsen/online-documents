function f_d = eval_f(x,u,Ts)
    %% Parameters
    g    = 9.81;
    m    = 1.4;   % [kg]         Drone mass
    d    = 0.2;   % [m]          Distance motor - drone center
    Ix   = 0.03;  % [kg*m^2]     Moment of inertia around x-axis (roll)
    Iy   = 0.03;  % [kg*m^2]     Moment of inertia around y-axis (pitch)
    Iz   = 0.04;  % [kg*m^2]     Moment of inertia around z-axis (yaw)
    Kyaw = 4;     % [rad*m]      Constant relating propulsion force to propeller moment on frame
    
    % States
%     px = x(1); py = x(2); pz = x(3);
    vx = x(4); vy = x(5); vz = x(6);
    phi   = x(7); theta = x(8); psi   = x(9);
    p = x(10); q = x(11); r = x(12);
    % Controls
    F1 = u(1); F2 = u(2); F3 = u(3); F4 = u(4);
    
    % Nonlinear, continuous-time model
    xdot = zeros(size(x));
    xdot(1) = vx;
    xdot(2) = vy;
    xdot(3) = vz;
    xdot(4) = 1/m*(F1 + F2 + F3 + F4)*(cos(phi)*sin(theta)*cos(psi) + sin(phi)*sin(psi));
    xdot(5) = 1/m*(F1 + F2 + F3 + F4)*(cos(phi)*sin(theta)*sin(psi) - sin(phi)*cos(psi));
    xdot(6) = 1/m*(F1 + F2 + F3 + F4)*(cos(phi)*cos(theta)) - g;
    xdot(7) = p;
    xdot(8) = q;
    xdot(9) = r;
    xdot(10) = d/Ix*(F3-F4) + (Iy-Iz)/Ix*q*r;
    xdot(11) = d/Iy*(F1-F2) + (Iz-Ix)/Iy*r*p;
    xdot(12) = Kyaw/Iz*(F1+F2-F3-F4)+(Ix-Iy)/Iz*p*q;

    
    %% Discretize with Forward Euler scheme
    f_d = x + Ts*xdot;    

end