% Exercise session 5:
% The (Extended) Kalman filter
% 2020
% ============================

close all
clear variables
addpath(genpath('simulation'))


%% Settings
animation = true;
figures = true;                  % Display figures.
convergence_consistency = false;  % Display states with their confidence intervals and a consistency check.
use_estimate = false;             % Perform feedback commands based on state estimate instead of simulated state.


%% Parameters, constants, reference, vectors
g    = 9.81;  % [m/s^2]      Gravitational acceleration
m    = 1.4;   % [kg]         Drone mass
d    = 0.2;   % [m]          Distance motor - drone center
Ix   = 0.03;  % [kg*m^2]     Moment of inertia around x-axis (roll)
Iy   = 0.03;  % [kg*m^2]     Moment of inertia around y-axis (pitch)
Iz   = 0.04;  % [kg*m^2]     Moment of inertia around z-axis (yaw)
Kyaw = 4;     % [rad*m]      Constant relating propulsion force to propeller moment on frame

Tsim = 5;    % [s]
Ts   = 0.01;  % [s]
N    = Tsim/Ts;

% Equilibrium & reference state
ustar = m*g/4*ones(4,1);
xstar = [0 0 1.5 0 0 0 0 0 0 0 0 0]'; % [px py pz vx vy vz phi theta psi p q r]' 

% Initialization of data structures
u      = zeros(4,N);  % [F1 F2 F3 F4]'
u(:,1) = ustar;
ymeas  = zeros(6,N);  % [px py pz phi theta psi]'

xhat_lqe = zeros(12,N);
% xhat_kf  = zeros(?,N);
% xhat_ekf = zeros(?,N);
Phat_lqe = zeros(12,12,N);
% Phat_kf  = zeros(?,?,N);
% Phat_ekf = zeros(?,?,N);
nu_lqe   = zeros(6,N);
% nu_kf    = zeros(?,N);
% nu_ekf   = zeros(?,N);
Slqe     = zeros(6,6,N);
% Skf      = zeros(?,?,N);
% Sekf     = zeros(?,?,N);
% Lkf      = zeros(?,?,N);
% Lekf     = zeros(?,?,N);

           
%% Controller and estimator design

% Linearized state space model
% ----------------------------
%     dx_dot = F*dx + G*du , 
%     dx = [dpx, dpy, dpz, dvx, dvy, dvz, dphi, dtheta, dpsi, dp, dq, dr]'
[F, G] = eval_jacobians(xstar, ustar, Ts);
C = [zeros(3), eye(3), zeros(3, 6);
    zeros(3, 9), eye(3)]; 
D = zeros(6);


% Estimator design
% ----------------
% rho = 1e-4;
rho = 1e-7;
% rho = 1e-9;

Qest = rho*diag([1,    1,    1e-2,...   % px py pz
                 1e3,  1e3,  1e4,...   % vx vy vz
                 1e-1, 1e-1, 1,...  % phi theta psi
                 1e2,  1e2,  1e2]);    % p q r
Rest = diag([1e-4, 1e-4, 1e-4, 1e-6, 1e-6, 1e-6]);
% Rest = 1e2*diag([1e-4, 1e-4, 1e-4, 1e-6, 1e-6, 1e-6]);  % Overestimate noise
% Rest = 1e-2*diag([1e-4, 1e-4, 1e-4, 1e-6, 1e-6, 1e-6]);  % Underestimate noise

% - LQE
Llqe = dlqr(F',F'*C',Qest,Rest)';

% - (E)KF
% Use Qest, Rest or tune them differently.



% Initial estimator guess
% -----------------------
xhat_lqe(:,1)   = xstar; dxhat_lqe(:,1) = xhat_lqe(:,1) - xstar;
% xhat_kf(:,1)    = ?; dxhat_kf(:,1)  = xhat_kf(:,1) - xstar;
% xhat_ekf(:,1)   = ?; dxhat_ekf(:,1)  = xhat_ekf(:,1) - xstar;

Phat_lqe(:,:,1) = zeros(12);
% Phat_kf(:,:,1)  = ?;
% Phat_ekf(:,:,1) = ?;


%% Simulation

run simulation/initialize_LQE_KF_EKF.m

for i=2:N
    
    % Plant simulation
    % ----------------
    sim.x(:,i) = simulate(sim.x(:,i-1),u(:,i-1));
  
    % Measurements
    % ------------
    ymeas(:,i) = get_meas(sim.x(:,i)); % [px py pz phi theta psi]'
    
    % Estimator
    % ---------
    % IMPLEMENT YOUR ESTIMATORS HERE!
    % =====================================================================
    % LQE
    % Estimator prediction
    dxhat_lqe(:,i) = F*xhat_lqe(:,i-1) + G*(u(:,i-1) - ustar);
    % Estimator correction
    nu_lqe(:,i) = ymeas(:,i) - C*(dxhat_lqe(:,i) + xstar);
    dxhat_lqe(:,i) = dxhat_lqe(:,i) + Llqe*nu_lqe(:,i);
    xhat_lqe(:,i) = dxhat_lqe(:,i) + xstar;
    
    % KF
    % ...
    
    % EKF
    % dfdx = eval_jacobians(xhat_ekf(:,i-1),u,Ts);
    % ...
    
    % =====================================================================    
    
    
    % Controller action
    % -----------------
    if use_estimate
        u(:,i) = K*(dxref(:,i) - dxhat_lqe(:,i)) + ustar;
%         u(:,i) = K*(dxref(:,i) - dxhat_lqe(:,i)) + ustar;
%         u(:,i) = K*(dxref(:,i) - dxhat_kf(:,i)) + ustar;
        
    else
        u(:,i) = K*(dxref(:,i) - (sim.x(:,i)-xstar)) + ustar;
    end
          
end


%% Visualization
axislimits = [-2 2 -2 2 0 4];
if animation
    animate(sim.x,params,colors,axislimits)
end

% Figures
if figures
    
    % Verify that Lkf and Lekf converge towards Llqe - here only for one
    % term, other terms analogous.
    figure()
    hold on
    plot(sim.t,squeeze(Llqe(1,1).*ones(size(Lkf(1,1,:)))),"Color",colors.red)
    plot(sim.t,squeeze(Lkf(1,1,:)),"x", "Color",colors.green)
    plot(sim.t,squeeze(Lekf(1,1,:)),"o","Color",colors.green2)
    legend("L_{lqe,1,1}", "L_{kf,1,1}", "L_{ekf,1,1}")
    xlabel("Time (s)");

    % Plot states and measurements
    % p_x (state 1)
    figure()
    hold on
    subplot(2,3,1)
    hold on
    plotx = plot(sim.t, ymeas(1,:),'Color',colors.yellow); plotx.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(1,:),"Color",colors.red)
    plot(sim.t, xhat_kf(1,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(1,:),"Color",colors.green2)
    title("x-Position")
    xlabel("Time (s)")
    ylabel("Position (m)")
    legend("x-position measurement",...
           "x-position LQE estimate","x-position KF estimate","x-position EKF estimate") %,"x-position reference (m)")
    title('x-position')
    
    % v_x (state 4)
    subplot(2,3,4)
    hold on
    hold on
    plotvx = plot(sim.t, gradient(ymeas(1,:))/Ts,"Color",colors.yellow); plotvx.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(4,:),"Color",colors.red)
    plot(sim.t, xhat_kf(4,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(4,:),"Color",colors.green2)
    title("x-Velocity")
    xlabel("Time (s)")
    ylabel("Velocity (m/s)")
    legend("v_x finite difference",...
           "v_x LQE estimate","v_x KF estimate","v_x EKF estimate")
    title('x-velocity')
    
    % p_y (state 2)
    subplot(2,3,2)
    hold on
    ploty = plot(sim.t, ymeas(2,:),'Color',colors.yellow); ploty.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(2,:),"Color",colors.red)
    plot(sim.t, xhat_kf(2,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(2,:),"Color",colors.green2)
    title("y-Position")
    xlabel("Time (s)")
    ylabel("Position (m)")
    legend("y position measurement",...
           "y position LQE estimate (m)","y position KF estimate (m)","y position EKF estimate (m)")
    title('y-position')
    
    % v_y (state 5)
    subplot(2,3,5)
    hold on
    plotvy = plot(sim.t, gradient(ymeas(2,:))/Ts,"Color",colors.yellow); plotvy.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(5,:),"Color",colors.red)
    plot(sim.t, xhat_kf(5,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(5,:),"Color",colors.green2)
    title("y-Velocity")
    xlabel("Time (s)")
    ylabel("v_y (m/s)")
    legend("v_y finite difference (m/s)",...
           "v_y LQE estimate (m/s)","v_y KF estimate (m/s)","v_y EKF estimate (m/s)")
    title('y-velocity')
    
    % p_z (state 3)
    subplot(2,3,3)
    hold on
    plotz = plot(sim.t, ymeas(3,:),"Color",colors.yellow); plotz.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(3,:),"Color",colors.red)
    plot(sim.t, xhat_kf(3,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(3,:),"Color",colors.green2)
    title("Altitude")
    xlabel("Time (s)")
    ylabel("Altitude (m)")
    legend("Altitude measurement (m)",...
           "Altitude LQE estimate (m)","Altitude KF estimate (m)","Altitude EKF estimate (m)")
    title('z-position')
    
    % v_z (state 6)
    subplot(2,3,6)
    hold on
    plotvz = plot(sim.t, gradient(ymeas(3,:))/Ts,"Color",colors.yellow); plotvz.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(6,:),"Color",colors.red)
    plot(sim.t, xhat_kf(6,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(6,:),"Color",colors.green2)
    xlabel("Time (s)")
    ylabel("v_z (m/s)")
    legend("v_z finite difference (m/s)",...
           "v_z LQE estimate (m/s)","v_z KF estimate (m/s)","v_z EKF estimate (m/s)") 
    title('z-velocity')  
       
    % roll (state 7)
    figure()
    hold on
    subplot(2,3,1)
    hold on
    plotroll = plot(sim.t, ymeas(4,:),'Color',colors.yellow); plotroll.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(7,:), 'Color',colors.red)
    plot(sim.t, xhat_kf(7,:), 'Color',colors.green)
    plot(sim.t, xhat_ekf(7,:), 'Color',colors.green2)
    title("roll")
    xlabel("Time (s)")
    ylabel("Angle (rad)")
    legend("roll measurement",...
           "roll LQE estimate",...
           "roll KF estimate",...
           "roll EKF estimate")
    title('roll')

    % roll rate (state 10)
    subplot(2,3,4)
    hold on
    plotrr = plot(sim.t, gradient(ymeas(4,:))/Ts,"Color", colors.yellow); plotrr.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(10,:),"Color",colors.red)
    plot(sim.t, xhat_kf(10,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(10,:),"Color",colors.green2)
    title("roll rate")
    xlabel("Time (s)")
    ylabel("Angular rate (rad/s)")
    legend("roll rate finite difference",...
           "roll rate LQE estimate","roll rate KF estimate","roll rate EKF estimate")
    title('roll rate')
    
    % pitch (state 8)
    subplot(2,3,2)
    hold on
    plotpitch = plot(sim.t, ymeas(5,:), 'Color',colors.yellow); plotpitch.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(8,:), 'Color',colors.red)
    plot(sim.t, xhat_kf(8,:), 'Color',colors.green)
    plot(sim.t, xhat_ekf(8,:), 'Color',colors.green2)
    title("pitch")
    xlabel("Time (s)")
    ylabel("Angle (rad)")
    legend("pitch measurement",...
           "pitch LQE estimate",...
           "pitch KF estimate",...
           "pitch EKF estimate")
    title('pitch')

    % pitch rate (state 11)
    subplot(2,3,5)
    hold on
    plotpr = plot(sim.t, gradient(ymeas(5,:))/Ts,"Color", colors.yellow); plotpr.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(11,:),"Color",colors.red)
    plot(sim.t, xhat_kf(11,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(11,:),"Color",colors.green2)
    title("pitch rate")
    xlabel("Time (s)")
    ylabel("Angular rate (rad/s)")
    legend("pitch rate finite difference",...
           "pitch rate LQE estimate","pitch rate KF estimate","pitch rate EKF estimate")
    title('pitch rate')
    
    % yaw (state 9)
    subplot(2,3,3)
    hold on
    plotyaw = plot(sim.t, ymeas(6,:),'Color',colors.yellow); plotyaw.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(9,:),'Color',colors.red)
    plot(sim.t, xhat_kf(9,:),'Color',colors.green)
    plot(sim.t, xhat_ekf(9,:),'Color',colors.green2)
    title("yaw")
    xlabel("Time (s)")
    ylabel("Angle (rad)")
    legend("yaw measurement",...
           "yaw LQE estimate",...
           "yaw KF estimate",...
           "yaw EKF estimate")
    title('yaw')
    
    % yaw rate (state 12)
    subplot(2,3,6)
    hold on
    plotyr = plot(sim.t, gradient(ymeas(6,:))/Ts,'Color',colors.yellow); plotyr.Color(4) = 0.3;
    plot(sim.t, xhat_lqe(12,:),"Color",colors.red)
    plot(sim.t, xhat_kf(12,:),"Color",colors.green)
    plot(sim.t, xhat_ekf(12,:),"Color",colors.green2)
    title("yaw rate")
    xlabel("Time (s)")
    ylabel("Angular rate (rad/s)")
    legend("yaw rate finite difference",...
           "yaw rate LQE estimate","yaw rate KF estimate","yaw rate EKF estimate")
    title('yaw rate')
    
    % Control signals
    figure()
    hold on
    plot(sim.t,u(1,:),"Color",colors.red)
    plot(sim.t,u(2,:),"Color",colors.blue)
    plot(sim.t,u(3,:),"Color",colors.yellow)
    plot(sim.t,u(4,:),"Color",colors.green)
    xlabel("Time (s)")
    ylabel("Thrust force (N)")
    legend("motor 1","motor 3","motor 3","motor 4")

end


%% Analyze convergence & consistency
if convergence_consistency

    % "Small signal" analysis.
    du = u-ustar;
    dy = ymeas-C*xstar;
    
    R = repmat(R,[1 1 N]);
    
    % Create KalmanExperiment objects.
    ke_lqe = KalmanExperiment(sim.t',dxhat_lqe,Phat_lqe,dy,R,du,nu_lqe,Slqe);
    ke_kf  = KalmanExperiment(sim.t',dxhat_kf, Phat_kf, dy,R,du,nu_kf, Skf);
    ke_ekf = KalmanExperiment(sim.t',dxhat_ekf,Phat_ekf,dy,R,du,nu_ekf,Sekf);

    % Plot "small signal" states with their 95% confidence interval.
    % p_x
    figure()
    hold on
    plotstates(ke_lqe,1,0.95)
    plotstates(ke_kf, 1,0.95)
    plotstates(ke_ekf,1,0.95)
    xlabel('time'); ylabel('\delta p_x (m)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % p_y
    figure()
    hold on
    plotstates(ke_lqe,2,0.95)
    plotstates(ke_kf, 2,0.95)
    plotstates(ke_ekf,2,0.95)
    xlabel('time'); ylabel('\delta p_y (m)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % p_z
    figure()
    hold on
    plotstates(ke_lqe,3,0.95)
    plotstates(ke_kf, 3,0.95)
    plotstates(ke_ekf,3,0.95)
    xlabel('time'); ylabel('\delta p_z (m)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')
    
    % v_x
    figure()
    hold on
    plotstates(ke_lqe,4,0.95)
    plotstates(ke_kf, 4,0.95)
    plotstates(ke_ekf,4,0.95)
    xlabel('time'); ylabel('\delta v_x (m/s)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % v_y
    figure()
    hold on
    plotstates(ke_lqe,5,0.95)
    plotstates(ke_kf, 5,0.95)
    plotstates(ke_ekf,5,0.95)
    xlabel('time'); ylabel('\delta v_y (m/s)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % v_z
    figure()
    hold on
    plotstates(ke_lqe,6,0.95)
    plotstates(ke_kf, 6,0.95)
    plotstates(ke_ekf,6,0.95)
    xlabel('time'); ylabel('\delta v_z (m/s)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % phi roll
    figure()
    hold on
    plotstates(ke_lqe,7,0.95)
    plotstates(ke_kf, 7,0.95)
    plotstates(ke_ekf,7,0.95)
    xlabel('time'); ylabel('\delta \phi (rad)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % theta pitch
    figure()
    hold on
    plotstates(ke_lqe,8,0.95)
    plotstates(ke_kf, 8,0.95)
    plotstates(ke_ekf,8,0.95)
    xlabel('time'); ylabel('\delta \theta (rad)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % psi yaw
    figure()
    hold on
    plotstates(ke_lqe,9,0.95)
    plotstates(ke_kf, 9,0.95)
    plotstates(ke_ekf,9,0.95)
    xlabel('time'); ylabel('\delta \psi (rad)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % p roll rate
    figure()
    hold on
    plotstates(ke_lqe,10,0.95)
    plotstates(ke_kf, 10,0.95)
    plotstates(ke_ekf,10,0.95)
    xlabel('time'); ylabel('\delta p (rad/s)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % q pitch rate
    figure()
    hold on
    plotstates(ke_lqe,11,0.95)
    plotstates(ke_kf, 11,0.95)
    plotstates(ke_ekf,11,0.95)
    xlabel('time'); ylabel('\delta q (rad/s)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')

    % r yaw rate
    figure()
    hold on
    plotstates(ke_lqe,12,0.95)
    plotstates(ke_kf, 12,0.95)
    plotstates(ke_ekf,12,0.95)
    xlabel('time'); ylabel('\delta r (rad/s)');
    legend('LQE, 95% confidence region', 'KF, 95% confidence region', 'EKF, 95% confidence region')


    % Consistency
    ke_lqe.analyzeconsistency();
    ke_kf.analyzeconsistency();
    ke_ekf.analyzeconsistency();
    
end

