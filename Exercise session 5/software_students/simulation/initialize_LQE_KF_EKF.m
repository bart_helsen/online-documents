colors.blue   = [0.3010, 0.7450, 0.9330];
colors.red    = [0.6350, 0.0780, 0.1840];
colors.yellow = [0.9290, 0.6940, 0.1250];
colors.green  = [0.1569, 0.7059, 0.2353];
colors.green2  = [0.0569, 0.5059, 0.0353];

set(0, 'DefaultLineLineWidth', 1.5);

disturbance = false;              % Add random wind disturbance.


%% Parameters

params.g    = 9.81;  % [m/s^2]      Gravitational acceleration
params.m    = 1.4;   % [kg]         Drone mass
params.d    = 0.2;   % [m]          Distance motor - drone center
params.Ix   = 0.03;  % [kg*m^2]     Moment of inertia around x-axis (roll)
params.Iy   = 0.03;  % [kg*m^2]     Moment of inertia around y-axis (pitch)
params.Iz   = 0.04;  % [kg*m^2]     Moment of inertia around z-axis (yaw)
params.Kyaw = 4;     % [m]          Constant relating propulsion force to propeller moment on frame

% Visualization drone geometry
params.fpz = 0.05;  % [m]
params.r   = 0.075; % [m]

% Added measurement noise
sim.Rmeas = diag([1e-4, 1e-4, 1e-4, 1e-6, 1e-6, 1e-6]);

% Simulation parameters
sim.t = 0:Ts:(N-1)*Ts;
sim.x = zeros(12,N);
params.Ts = Ts;
params.N = N;

% Initial conditions 
sim.x(1:3,1) = [1.1, 0.1, 0.53]'; % [px py pz]'
sim.x(7:9,1) = [0.13, 0.12, 0.09]';  % [phi theta psi]'

% First measurement
px    = sim.x(1,1);
py    = sim.x(2,1);
pz    = sim.x(3,1);
roll  = sim.x(7,1);
pitch = sim.x(8,1);
yaw   = sim.x(9,1);

ymeas(:,1) = [px, py, pz, roll, pitch, yaw]' + sqrt(sim.Rmeas)*randn(6,1);


%% Reference trajectory
x_pos_ref = 1*cos(2*sim.t);  % trajectory tracking problem
y_pos_ref = 1*sin(2*sim.t);
z_pos_ref = 1.5*ones(1,length(sim.t));
vx_ref = gradient(x_pos_ref)/Ts;
vy_ref = gradient(y_pos_ref)/Ts;
vz_ref = gradient(z_pos_ref)/Ts;
xref   = [x_pos_ref; y_pos_ref; z_pos_ref; vx_ref; vy_ref; vz_ref; zeros(6,N)];

% xref  = repmat(xstar,[1,N]);  % stabilization problem
dxref  = xref - xstar;


%% Nonlinear equations for simulation
% x_dot = f(x,u)
% x = [px, py, pz, vx, vy, vz, phi, theta, psi, p, q, r,]'
%       1   2   3   4   5   6   7      8    9  10  11 12
% Fd : Constant force disturbance
sim.Fd = zeros(3,1);
if disturbance
    sim.Fd(1:2) = 0.1*randn(2,1);
end

% state equation
sim.xdot = @(x,u) [x(4);
                   x(5);
                   x(6);
                   1/params.m*(u(1)+u(2)+u(3)+u(4))*(cos(x(7))*sin(x(8))*cos(x(9)) + sin(x(7))*sin(x(9))) + sim.Fd(1);
                   1/params.m*(u(1)+u(2)+u(3)+u(4))*(cos(x(7))*sin(x(8))*sin(x(9)) - sin(x(7))*cos(x(9))) + sim.Fd(2);
                   1/params.m*(u(1)+u(2)+u(3)+u(4))*(cos(x(7))*cos(x(8))) - params.g + sim.Fd(3);
                   x(10);
                   x(11);
                   x(12);
                   params.d/params.Ix*(u(3)-u(4)) + (params.Iy-params.Iz)/params.Ix*x(11)*x(12);
                   params.d/params.Iy*(u(1)-u(2)) + (params.Iz-params.Ix)/params.Iy*x(12)*x(10);
                   params.Kyaw/params.Iz*(u(1)+u(2)-u(3)-u(4)) + (params.Ix-params.Iy)/params.Iz*x(10)*x(11)];
           
           
simulate = @ (xsim,u) rk4(sim.xdot,xsim,u,Ts);
           
get_meas = @ (xsim)[xsim(1);
                    xsim(2);
                    xsim(3);
                    xsim(7);
                    xsim(8);
                    xsim(9)] + sqrt(sim.Rmeas)*randn(6,1);


%% Feedback controller design
% Linearized & discretized model
% - Jacobians evaluated in xstar, ustar
ctrl.F = zeros(12);  % 'ctrl' struct to make sure this F does not overwrite a possible F created in the exercise.
ctrl.F(4,8) = g;
ctrl.F(5,7) = -g;
ctrl.F(1,4)  = 1;
ctrl.F(2,5)  = 1;
ctrl.F(3,6)  = 1;
ctrl.F(7,10) = 1;
ctrl.F(8,11) = 1;
ctrl.F(9,12) = 1;

ctrl.G = zeros(12,4);
ctrl.G(6,:)   = 1/m;
ctrl.G(10,3:4) = [d/Ix, -d/Ix];
ctrl.G(11,1:2) = [d/Iy, -d/Iy];
ctrl.G(12,:)   = [Kyaw/Iz, Kyaw/Iz, -Kyaw/Iz, -Kyaw/Iz];

ctrl.C = zeros(6,12);
ctrl.C(1:3,1:3) = eye(3);  % y = [x y z phi theta psi]'
ctrl.C(4:6,7:9) = eye(3);
ctrl.D = 0;

% - Discretization
ctrl.F = Ts*ctrl.F + eye(size(ctrl.F));
ctrl.G = Ts*ctrl.G; 

% LQR
ctrl.Qlqr = diag([1e2, 1e2, 1e2,... % px py pz
                  1e1, 1e1, 1e1,... % vx vy vz
                  1e2, 1e2, 1e2,... % roll pitch yaw
                  1e0, 1e0, 1e0]);  % p q r

ctrl.Rlqr = 1*eye(4);

ctrl.Kdlqr = dlqr(ctrl.F,ctrl.G,ctrl.Qlqr,ctrl.Rlqr);
K = ctrl.Kdlqr;
                
