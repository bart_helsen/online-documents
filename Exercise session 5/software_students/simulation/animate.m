function animate(xsim, params, colors, axislimits)
    
    N = params.N;
    d = params.d;
    fpz = params.fpz;
    r = params.r;
    Ts = params.Ts;
    steps_per_frame = 5;

    figure(1);
    axis(axislimits);
    view(3);
    grid on    

    for i=2:N

        % Measurements
        % ------------
        px    = xsim(1,i);
        py    = xsim(2,i);
        pz    = xsim(3,i);
        roll  = xsim(7,i);
        pitch = xsim(8,i);
        yaw   = xsim(9,i);


        % Visualization
        % -------------
        if not(mod(i,steps_per_frame))  % Display only subset of all time data.

            Rx = [1,         0,          0;
                  0, cos(roll), -sin(roll);
                  0, sin(roll),  cos(roll)];
            Ry = [ cos(pitch), 0, sin(pitch);
                            0, 1,          0;
                  -sin(pitch), 0, cos(pitch)];
            Rz = [cos(yaw), -sin(yaw), 0;
                  sin(yaw),  cos(yaw), 0;
                  0,                0, 1];
            R = Rz*Ry*Rx; % Roll-pitch-yaw rotation matrix.

            fp1 = R*[[-d;0;fpz] [-d;0;0] [d; 0;0] [d; 0;fpz]];  % framepoints
            fp2 = R*[[0; d;fpz] [0; d;0] [0;-d;0] [0;-d;fpz]];
            frame = {[fp1(1,:); fp2(1,:)]', [fp1(2,:); fp2(2,:)]', [fp1(3,:); fp2(3,:)]'};

            t = 0:0.4:2*pi+0.4;
            rp1 = R*[ -d+r*cos(t);    r*sin(t); fpz*ones(1,length(t))];
            rp2 = R*[  d+r*cos(t);    r*sin(t); fpz*ones(1,length(t))];
            rp3 = R*[    r*cos(t);  d+r*sin(t); fpz*ones(1,length(t))];
            rp4 = R*[    r*cos(t); -d+r*sin(t); fpz*ones(1,length(t))];
            rotors = {{rp1(1,:),rp1(2,:),rp1(3,:)},...
                      {rp2(1,:),rp2(2,:),rp2(3,:)},...
                      {rp3(1,:),rp3(2,:),rp3(3,:)},...
                      {rp4(1,:),rp4(2,:),rp4(3,:)}};
            figure(1)
            clf
            view(3);
            axis(axislimits);

            grid on
            hold on
            plot3(px+frame{1},py+frame{2},pz+frame{3}, 'Color', colors.red)
            plot3(px+[rotors{1}{1};rotors{2}{1};rotors{3}{1};rotors{4}{1}]',...
                  py+[rotors{1}{2};rotors{2}{2};rotors{3}{2};rotors{4}{2}]',...
                  pz+[rotors{1}{3};rotors{2}{3};rotors{3}{3};rotors{4}{3}]', 'Color', colors.red)
            hold off
        end
        pause(Ts)  
    end

end