function [dfdx, dfdu] = eval_jacobians(x,u,Ts)
    %% Parameters
    m    = 1.4;   % [kg]         Drone mass
    d    = 0.2;   % [m]          Distance motor - drone center
    Ix   = 0.03;  % [kg*m^2]     Moment of inertia around x-axis (roll)
    Iy   = 0.03;  % [kg*m^2]     Moment of inertia around y-axis (pitch)
    Iz   = 0.04;  % [kg*m^2]     Moment of inertia around z-axis (yaw)
    Kyaw = 4;     % [rad*m]      Constant relating propulsion force to propeller moment on frame

    phi = x(7); theta = x(8); psi = x(9);
    p = x(10); q = x(11); r = x(12);
    
    F1 = u(1); F2 = u(2); F3 = u(3); F4 = u(4);

    

    %% Jacobian wrt states df/dx
    dfdx = zeros(12,12);
    dfdx(1:3,4:6) = eye(3);
    dfdx(4:6,7:9) = 1/m*(F1 + F2 + F3 + F4)*...
        [-sin(phi)*sin(theta)*cos(psi)+cos(phi)*sin(psi), cos(phi)*cos(theta)*cos(psi), -cos(phi)*sin(theta)*sin(psi)+sin(phi)*cos(psi);
         -sin(phi)*sin(theta)*sin(psi)-cos(phi)*cos(psi), cos(phi)*cos(theta)*sin(psi),  cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi);
         -sin(phi)*cos(theta), -cos(phi)*sin(theta), 0];
    dfdx(7:9,  10:12) = eye(3);
    dfdx(10:12,10:12) = [0,            (Iy-Iz)/Ix*r, (Iy-Iz)/Ix*q;
                         (Iz-Ix)/Iy*r,            0, (Iz-Ix)/Iy*p;
                         (Ix-Iy)/Iz*q, (Ix-Iy)/Iz*p,            0];

    
    %% Jacobian wrt inputs df/du
    dfdu = zeros(12,4);
    dfdu(4:6,:) = 1/m*[cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi);
                       cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi);
                       cos(phi)*cos(theta)]*ones(1,4);
    dfdu(10:12,:) = [0,             0,     d/Ix,    -d/Ix;
                     d/Iy,      -d/Iy,        0,        0;
                     Kyaw/Iz, Kyaw/Iz, -Kyaw/Iz, -Kyaw/Iz];
    

    %% Discretize with Forward Euler scheme
    dfdx = eye(12) + Ts*dfdx;
    dfdu = Ts*dfdu;

end